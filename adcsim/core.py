import numpy as np


class AngularMomentum:
    """Return angular momentum of actuator"""

    def __init__(self, angular_momentum):
        self._angular_momentum = angular_momentum

    @property
    def angular_momentum(self):
        return self._angular_momentum


class Differentiable:
    """Return derivative of state vector"""

    def __init__(self):
        self._state = None

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, new):
        self._state = new

    def derivative(self):
        raise NotImplementedError


class Input:
    """Set input variables of actuators"""

    def __init__(self, input=None):
        if type(input) is not np.ndarray:
            self._input = np.array(input)
        else:
            self._input = input

        # TODO: Add setting lower and upper bounds for the input vector

    @property
    def input(self):
        return self._input

    @input.setter
    def input(self, value):
        self._input = value

        # TODO: Raise error if value is out of bounds.


class Torque:
    """Interface to access torque"""

    def __init__(self):
        self._torque = 0

    @property
    def torque(self):
        return self._torque
