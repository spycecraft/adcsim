from adcsim.core import Differentiable
from pyquaternion import Quaternion
from collections import OrderedDict

import numpy as np


class QuaternionKinematics(Differentiable):
    """Quaternion kinematics"""

    _quaternion: Quaternion

    def __init__(self, quaternion=Quaternion([1, 0, 0, 0])):
        self._quaternion = quaternion

    @property
    def attitude(self):
        """Attitude as `pyquaternion.Quaternion`."""
        return self._quaternion

    @property
    def state(self):
        return self._quaternion.elements

    @state.setter
    def state(self, new):
        self._quaternion = Quaternion(new)

    def derivative(self, angular_rate):
        return self._quaternion.derivative(angular_rate).elements


class RigidBodyDynamics(Differentiable):
    """Rigid body dynamics"""

    def __init__(self, inertia=np.eye(3), angular_rate=np.array([0, 0, 0])):
        # TODO check dimensions of angular rate and inertia
        # TODO check entries in inertia matrix
        self._inertia = inertia
        self._inverse_inertia = np.linalg.inv(inertia)
        self._angular_rate = angular_rate

    @property
    def angular_rate(self):
        """Rigid body angular rate as 3x1 `numpy.ndarray`."""
        return self._angular_rate

    @property
    def inertia(self):
        return self._inertia

    @inertia.setter
    def inertia(self, new):
        self._inertia = new
        self._inverse_inertia = np.linalg.inv(new)

    @property
    def state(self):
        return self._angular_rate

    @state.setter
    def state(self, new):
        self._angular_rate = new

    def derivative(self, torque, angular_momentum):
        return self._inverse_inertia @ (
            torque
            - np.cross(
                self._angular_rate,
                (self._inertia @ self._angular_rate) + angular_momentum,
            )
        )
