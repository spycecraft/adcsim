from collections import OrderedDict
import numpy as np

from .core import AngularMomentum, Differentiable, Input, Torque


class Actuator(Input, Torque):
    """Actuator base class"""

    def __init__(self, alignment=np.array([0, 0, 1]), input=0):
        Input.__init__(self, input)
        Torque.__init__(self)

        # TODO: Check if alignment is a 3D unit vector before setting it.
        self._alignment = alignment

    def __str__(self):
        return (
            f"Actuator\n"
            f"  alignment: {self.alignment}\n"
            f"  torque: {self.torque}\n"
            f"  input: {self.input}"
        )

    @property
    def alignment(self):
        return self._alignment

    @alignment.setter
    def alignment(self, alignment):
        # TODO: Check if alignment is a 3D unit vector before setting it.
        self._alignment = alignment

    @property
    def torque(self):
        """Transform the scalar torque using the alignment vector."""
        return self._torque * self._alignment


class Magnetorquer(Actuator):
    """Magnetorquer"""

    def __init__(self, alignment=np.array([0, 0, 1]), magnetic_dipole=0, input=[0]):
        Actuator.__init__(self, alignment, input)
        self.magnetic_dipole = magnetic_dipole

    def __str__(self):
        return (
            Actuator.__str__(self).replace("Actuator", "Fluid-Dynamic Actuator")
            + f"\n  magnetic_dipole: {self._magnetic_dipole}"
        )

    @property
    def magnetic_dipole(self):
        return self._magnetic_dipole

    @magnetic_dipole.setter
    def magnetic_dipole(self, new):
        # TODO implement input checking
        self._magnetic_dipole = new


class FluidDynamicActuator(Actuator, AngularMomentum, Differentiable):
    """Fluid-dynamic actuator"""

    def __init__(
        self,
        alignment=np.array([0, 0, 1]),
        gain=80e-6,
        time_constant=0.5,
        angular_momentum=0,
        internal_state=0,
        input=[0],
    ):
        Actuator.__init__(self, alignment, input)
        AngularMomentum.__init__(self, angular_momentum)

        self._gain = gain
        self._time_constant = time_constant

        self._internal_state = internal_state

    def __str__(self):
        return (
            Actuator.__str__(self).replace("Actuator", "Fluid-Dynamic Actuator")
            + f"\n  gain: {self._gain}\n  time constant: {self._time_constant}"
        )

    @property
    def derivative(self):
        state_derivative = [
            -1 / self._time_constant * self._angular_momentum
            + self._gain / self._time_constant * self.input,
            -1 / self._time_constant * self._internal_state
            - self._gain / self._time_constant ** 2 * self.input,
        ]
        return np.squeeze(state_derivative, axis=1)

    @property
    def torque(self):
        self._torque = (
            self._internal_state + self._gain / self._time_constant * self.input
        )
        return self._torque * self._alignment

    @property
    def angular_momentum(self):
        return self._angular_momentum * self.alignment

    @property
    def state(self):
        return np.hstack((self._angular_momentum, self._internal_state))

    @state.setter
    def state(self, new):
        self._angular_momentum = new[0]
        self._internal_state = new[1]
