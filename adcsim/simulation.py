import ciso8601
import datetime
import importlib.resources as importlib_resources
import json
import numpy as np
import time
import logging

from pyquaternion import Quaternion
from scipy.integrate import solve_ivp

from .actuator import FluidDynamicActuator, Magnetorquer
from .motion import QuaternionKinematics, RigidBodyDynamics

logging.basicConfig(
    filename="simulation.log", level=logging.DEBUG, format="%(asctime)s %(message)s"
)


class DifferentialEquationSolver:
    """Differential equation solver.

    :param string begin: Simulation start date and time in datetime format.
    :param string end: Simulation end date and time in datetime format.
    :param float time_step: Simulation time step.
    :param pyquaternion.Quaternion attitude: Initial spacecraft attitude quaternion.
    :param numpy.ndarray angular_rate: Initial spacecraft angular rate vector.
    :param nump.ndarray inertia: Spacecraft inertia matrix.
    """

    def __init__(
        self,
        begin="2022-01-01T00:00:00Z",
        end="2022-01-01T00:00:10",
        time_step=0.5,
        evaluation_steps=0,
        attitude=Quaternion((1, 0, 0, 0)),
        angular_rate=np.array((0, 0, 0)),
        inertia=np.eye(3),
    ):
        self.begin = begin
        self.end = end
        self.time_step = time_step
        self.evaluation_steps = evaluation_steps

        self._kinematics = QuaternionKinematics(attitude)
        self._dynamics = RigidBodyDynamics(inertia, angular_rate)
        self._actuators = []

        self.assemble_initial_state()
        self.assemble_initial_input()

    def _inertia_from_file(self, inertia):
        return np.array(
            [
                [inertia["xx"], inertia["xy"], inertia["zx"]],
                [inertia["xy"], inertia["yy"], inertia["yz"]],
                [inertia["zx"], inertia["yz"], inertia["zz"]],
            ]
        )

    @property
    def actuators(self):
        """
        The actuators as list of `adcsim.actuator`.
        """
        return self._actuators

    @property
    def angular_rate(self):
        """The spacecraft angular rate vector as `numpy.ndarray`"""
        return self._dynamics.angular_rate

    @property
    def attitude(self):
        """The spacecraft attitude as `pyquaternion.Quaternion`."""
        return self._kinematics.attitude

    @property
    def begin(self):
        """The simulation start time as a `time.datetime`."""
        return self._begin

    @begin.setter
    def begin(self, new):
        assert isinstance(ciso8601.parse_datetime(new), datetime.datetime)
        self._begin = ciso8601.parse_datetime(new)

    @property
    def end(self):
        """The simulation end time as a `time.datetime`."""
        return self._end

    @end.setter
    def end(self, new):
        assert isinstance(ciso8601.parse_datetime(new), datetime.datetime)
        self._end = ciso8601.parse_datetime(new)

    @property
    def evaluation_steps(self):
        """The number of intermediate evaluation steps within one control cycle.

        The step number is limited to a range 0 <= n < 10.
        """
        return self._evaluation_steps

    @evaluation_steps.setter
    def evaluation_steps(self, new):
        assert isinstance(new, int)
        assert new >= 0
        assert new < 10
        self._evaluation_steps = new

    @property
    def inertia(self):
        return self._dynamics.inertia

    @property
    def state(self):
        return self._state

    @state.setter
    def state(self, new):
        # TODO: Check inputs!
        self._state = new

    @property
    def time_step(self):
        return self._time_step

    @time_step.setter
    def time_step(self, new):
        assert isinstance(new, float)
        if new <= 0:
            raise ValueError("Time step must be positive.")
        self._time_step = new

    def assemble_initial_state(self):
        self.state = np.hstack((self._kinematics.state, self._dynamics.state))
        for actuator in self._actuators:
            if hasattr(actuator, "state"):
                self.state = np.hstack((self.state, actuator.state))

    def assemble_initial_input(self):
        self.inputs = np.array((None,))
        for actuator in self._actuators:
            if hasattr(actuator, "input"):
                if self.inputs[0] == None:
                    self.inputs = np.array(actuator.input)
                else:
                    self.inputs = np.hstack((self.inputs, actuator.input))

    @classmethod
    def from_file(cls, user_file_name=None):
        """Load simulation definition from user file"""

        with importlib_resources.path("adcsim", "default_params.json") as p:
            with open(p, "r") as f:
                parameters = json.load(f)

        # TODO override with additional user file parameters

        # Read simulation settings
        ulf = cls(
            begin=parameters["simulation"]["begin"],
            end=parameters["simulation"]["end"],
            time_step=float(parameters["simulation"]["time_step"]),
            attitude=Quaternion(parameters["rigid_body"]["quaternion"]),
            inertia=cls._inertia_from_file(cls, parameters["rigid_body"]["inertia"]),
            angular_rate=np.array(parameters["rigid_body"]["angular_rate"]),
            evaluation_steps=int(parameters["simulation"]["evaluation_steps"]),
        )

        # Log: equations of motion established
        logging.info("Equations of motion successfully established")

        # read actuators from the JSON object
        for actuator in parameters["actuators"]:
            if actuator["class"] == "FluidDynamicActuator":
                ulf._actuators.append(
                    FluidDynamicActuator(
                        np.array(actuator["alignment"]),
                        actuator["gain"],
                        actuator["time_constant"],
                        actuator["angular_momentum"],
                        actuator["internal_state"],
                        actuator["input"],
                    )
                )
            elif actuator["class"] == "Magnetorquer":
                ulf._actuators.append(
                    Magnetorquer(
                        np.array(actuator["alignment"]),
                        actuator["magnetic_dipole"],
                        actuator["input"],
                    )
                )
            else:
                actuator_class = actuator["class"]
                raise ValueError(f"Unknown actuator class {actuator_class}")

        # Log: Actuators established
        logging.info("Actuators successfully established")

        # TODO read perturbations from the JSON object

        # Log: Perturbations inputted
        logging.info("Perturbations inputted successfully")

        # assemble initial state and input vector
        ulf.assemble_initial_state()
        ulf.assemble_initial_input()

        return ulf

    def rhs(self, t, state):
        """Return the derivative of the state vector.

        :param float t: Simulation time
        :param array_like state: System state vector
        """
        # Disassemble the state vector and distribute it to
        # - kinematics
        # - dynamics
        # - actuators
        self._kinematics.state = state[0:4]
        self._dynamics.state = state[4:7]

        state_index = 7
        input_index = 0
        torques = np.zeros(3)
        angular_momenta = np.zeros(3)

        for actuator in self.actuators:
            # Assign actuator states
            if hasattr(actuator, "state"):
                i = range(state_index, state_index + len(actuator.state))
                actuator.state = state[i]
                state_index += len(actuator.state)

            # Update actuator inputs
            i = range(input_index, input_index + len(actuator.input))
            actuator.input = self.inputs[i]
            input_index += len(actuator.input)

            # Sum actuator torques
            if hasattr(actuator, "torque"):
                torques += actuator.torque

            # Sum actuator angular momenta
            if hasattr(actuator, "angular_momentum"):
                angular_momenta += actuator.angular_momentum

        # TODO Update perturbations and sum torques as well as angular momenta

        # Calculate state derivative
        quaternion_derivative = self._kinematics.derivative(self._dynamics.state)
        angular_rate_derivative = self._dynamics.derivative(torques, angular_momenta)

        state_derivative = np.hstack((quaternion_derivative, angular_rate_derivative))

        for actuator in self._actuators:
            if hasattr(actuator, "state"):
                state_derivative = np.hstack((state_derivative, actuator.derivative))

        return state_derivative

    def run(self):
        """
        Run the simulation.

        :return: tuple (time, trajectory) The time vector and 2D result vector.
        """
        begin_time = time.mktime(self.begin.timetuple())
        end_time = time.mktime(self.end.timetuple())

        # Get number of steps.
        number_of_steps = int((end_time - begin_time) / self.time_step)
        logging.info(f"Number of steps: {number_of_steps}")

        logging.info("Simulation is starting...")
        total_start_time = time.time()

        # Initialize output variables
        state_size = self.state.size
        result = self.state.reshape((state_size, 1))
        t_result = [begin_time]

        # Loop over steps.
        for i in range(number_of_steps):
            start_time = time.time()

            # Update time span.
            time_span = [
                begin_time + i * self.time_step,
                begin_time + (i + 1) * self.time_step,
            ]

            # Update evaluation times.
            if self.evaluation_steps > 0:
                t_eval = np.linspace(
                    time_span[0], time_span[1], self.evaluation_steps + 2
                )
            else:
                t_eval = None

            # TODO Update inputs.

            # Solve for current time span.
            solution = solve_ivp(
                self.rhs, time_span, self.state, method="RK45", atol=1e-6, t_eval=t_eval
            )

            if t_eval is None:
                solution_state_vector = solution.y[:, -1].reshape((state_size, -1))
                solution_time_vector = [solution.t[-1]]
            else:
                # Do not append the complete time and solution vectors, as the first
                # value in each vector is the last from the previous simulation step.
                solution_state_vector = solution.y[:, 1:].reshape((state_size, -1))
                solution_time_vector = solution.t[1:]

            result = np.hstack((result, solution_state_vector))
            t_result = np.concatenate((t_result, solution_time_vector))

            # Update state.
            self.state = solution.y[:, -1]

            # TODO Update outputs.

            # TODO Log inputs, state vector, and outputs. Look into `SpyceCraft/adcsim/acsim/library/util/logger.py` to understand what Lennart did in his thesis.

            # TODO Call next step of on-board software.

            logging.info(
                f"Step {i} duration: {1000*(time.time() - start_time):7.3f} ms"
            )

        logging.info(f"Duration: {1000*(time.time() - total_start_time):7.3f} ms")

        return t_result, result

    # TODO Implement __str__().
    def __str__(self):
        return "Differential Equation Solver"
