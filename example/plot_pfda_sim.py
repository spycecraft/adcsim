from matplotlib import pyplot as plt
import numpy as np
import argparse

from adcsim.simulation import DifferentialEquationSolver

parser = argparse.ArgumentParser(description="Run an adcsim simulation")
parser.add_argument(
    "--file", default="adcsim/default_params.json", help="simulation configuration file"
)
args = parser.parse_args()

solver = DifferentialEquationSolver.from_file(args.file)
t, y = solver.run()

angular_rate = y[:, 4:7]

plt.plot(t, np.rad2deg(angular_rate))
plt.xlabel("time [s]")
plt.ylabel("angular velocity [deg/s]")
plt.legend(["${_b}\omega_x$", "${_b}\omega_y$", "${_b}\omega_z$"])
plt.show()
