"""Actuator Class.

This class provides an actuator class for attitude control,
"""

import numpy as np
from pyquaternion import Quaternion

from ..util import load_params


class Actuator():
    """Actuator class for angular momentum exchange based actuators."""

    def __init__(self, angular_momentum=0,
                 torque=0,
                 sys_input=0):
        """Instanciate Actuator object.

        Parameters
        angular_momentum: int
            Angular momentum of the actuator
        torque: int
            Torque of the actuator
        sys_input: int
            System input in range of -1 to 1
        """

        self.update_angular_momentum(np.array([angular_momentum]))
        self.update_torque(np.array([torque]))
        self.set_input(sys_input)
        # self.update_state(angular_momentum, torque)

    def update_angular_momentum(self, angular_momentum):
        """Update the actuator angular momentum."""
        self.__H = angular_momentum

    def update_torque(self, torque):
        """Update the actuator torque."""
        self.__tau = torque

    def set_input(self, sys_input):
        """Set the system input of the actuator.

        Parameters
        sys_input: int
            System input in range of -1 to 1
        """
        if sys_input > 1:
            self.__u = 1
        elif sys_input < -1:
            self.__u = -1
        else:
            self.__u = sys_input

    def get_input(self):
        """Return system input."""
        return self.__u

    def get_angular_momentum(self):
        """Return angular momentum."""
        return self.__H

    def get_torque(self):
        """Return torque."""
        return self.__tau
