"""Utility functions for the simulation."""

import sys

class ProgressBar():
    """Progress bar object."""

    def __init__(self, endvalue, bar_length=35):
        """Initialize progress bar."""
        self.__endvalue = endvalue
        self.__bar_length = bar_length

    def print_progress_bar(self, value):
        """Print progress bar."""
        percent = float(value) / self.__endvalue
        arrow = '-' * int(round(percent * self.__bar_length)-1) + '>'
        spaces = ' ' * (self.__bar_length - len(arrow))

        sys.stdout.write("\rProgress: [{0}] {1}%".format(arrow + spaces,
                         int(round(percent * 100))))
        sys.stdout.flush()
