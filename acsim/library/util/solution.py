"""Utility functions for the simulation."""

import numpy as np
from .load_params import load_params


class Solution():
    """Solution class.

    Solution object which organizes a stream of data. This shall be used to
    store data in this object, save it to a file or forward it for live
    plotting for example.
    """

    def __init__(self, x0, ts=0, te=0, dt_eval=0, dt_solve=0):
        """Initialize solution object."""
        self.t = np.arange(ts, te+dt_eval, dt_eval)
        self.t_control = np.arange(ts, te, dt_solve)
        self.__y = np.array([x0])
        self.__u = np.array([0, 0, 0])

    @classmethod
    def from_parameter_file(cls, x0, fname):
        """Create solution object from parameter file."""
        params = load_params(fname)
        ts = params['simulation']['t_start']
        dt_solve = params['simulation']['dt_solve']
        dt_eval = params['simulation']['dt_eval']
        te = params['simulation']['t_end']

        result = cls(x0, ts=ts, te=te, dt_eval=dt_eval, dt_solve=dt_solve)
        return result

    @property
    def y(self):
        """Property for the state solution."""
        return self.__y

    @property
    def u(self):
        """Property for the control solution."""
        return self.__u

    def append_solution(self, y):
        """Append the state y to the solution."""
        self.__y = np.vstack((self.y, y))

    def append_control(self, u):
        """Append u to the control data."""
        self.__u = np.vstack((self.u, u))
