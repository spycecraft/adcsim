import numpy as np
import pandas as pd
import glob as gb
import os.path
from pyquaternion import Quaternion

from matplotlib import pyplot as plt

from ..util import load_params
from ..util import ProgressBar
from ..visualize import plot_result


class Result():
    def __init__(self, result_path, threshold=1):
        """Load a result directory and calulate figures of merit.
        
        Parameter:
        result_path: directory to the result files
        threshold: attitude error angle threshold

        Raises KeyError if no parameter file exists.
        """

        self.__dname = result_path
        self.__phi_n = threshold
        self.__init_parameter_data()
        self.__init_controller_data()
        self.__init_spacecraft_data()
        self.__init_actuator_data()
        self.__init_rate_error()
        self.__init_attitude_error()
        self.__init_attitude_error_angle()
        self.__mode = self.__param['trajectory']['mode']

        if self.__mode == 'manual':
            self.__init_fom_slew()

        if  self.__mode == 'nadirpointing':
            self.__init_fom_nadir()

        if  self.__mode == 'swathwidening':
            self.__init_fom_asi()

        if  self.__mode == 'stereoimaging':
            self.__init_fom_sssi()

    def __init_parameter_data(self):
        if os.path.isfile(self.__dname+"/parameter.json"):
            self.__param = load_params(self.__dname+"/parameter.json")
        else: 
            raise KeyError("No parameter file found in directory: {}."
                            .format(self.__dname))

    def __init_controller_data(self):
        if os.path.isfile(self.__dname+"/controller.csv"):
            self.__co_df = pd.read_csv(self.__dname+"/controller.csv", delimiter=',')

        else:
            self.__co_df = None

    def __init_spacecraft_data(self):
        if os.path.isfile(self.__dname+"/spacecraft.csv"):
            self.__sc_df = pd.read_csv(self.__dname+"/spacecraft.csv", delimiter=',')
        else:
            self.__sc_df = None

    def __init_actuator_data(self):
        if os.path.isfile(self.__dname+"/actuator.csv"):
            self.__ac_df = pd.read_csv(self.__dname+"/actuator.csv", delimiter=',')
        else:
            self.__ac_df = None

    def __init_rate_error(self):
        dt_solve = self.__param['simulation']['dt_solve']
        dt_eval  = self.__param['simulation']['dt_eval']

        w   = np.vstack((self.__sc_df['wx'],
                         self.__sc_df['wy'],
                         self.__sc_df['wz'])).transpose()

        w_c = np.vstack((self.__co_df['wx_c'],
                         self.__co_df['wy_c'],
                         self.__co_df['wz_c'])).transpose()

        w_e = np.array([0, 0, 0])
        for w, w_c in zip(w[0::int(dt_solve/dt_eval),:], w_c):
            temp = np.rad2deg(w - w_c)
            w_e = np.vstack((w_e, temp))

        self.__w_e = w_e.transpose()[:, 1:]

    def __init_attitude_error(self):
        dt_solve = self.__param['simulation']['dt_solve']
        dt_eval  = self.__param['simulation']['dt_eval']


        qib   = np.vstack((self.__sc_df['qs'],
                           self.__sc_df['qx'],
                           self.__sc_df['qy'],
                           self.__sc_df['qz'])).transpose()

        qib_c = np.vstack((self.__co_df['qs_c'],
                           self.__co_df['qx_c'],
                           self.__co_df['qy_c'],
                           self.__co_df['qz_c'])).transpose()

        q_e = np.array([0, 0, 0, 0])
        for q, q_c in zip(qib[0::int(dt_solve/dt_eval),:], qib_c):
            temp = Quaternion(q) * Quaternion(q_c).inverse
            q_e = np.vstack((q_e, temp.q))

        self.__q_e = q_e.transpose()[:, 1:]

    def __init_attitude_error_angle(self):
        phi_e = np.array([])
        for qe in self.__q_e.transpose():
            temp = np.rad2deg(Quaternion(qe).angle)
            phi_e = np.hstack((phi_e,temp))

        self.__phi_e = phi_e

    def _find_threshold_crossing(self, phi_e, t):
        """
        Return the index at which the error threshold is crossed 
        from above for the last time in the given angles phe_e.

        Return None if the error threshold is not reached.
        """
        try:
            if any(angle < self.__phi_n for angle in np.abs(phi_e)):
                if any(angle > self.__phi_n for angle in np.abs(phi_e)):
                    index = np.where(np.abs(phi_e) > self.__phi_n)[0][-1]
                else:
                    index = 0
            else:
                index = None
        except IndexError:
            index = None

        return index

    def __calculate_alpha(self, t_n, t_N, dt, phi_e):
        """Calculate figure of merit alpha."""
        alpha = (dt / (t_N - t_n)) * np.sum(phi_e**2)
        return alpha

    def __calculate_nu(self, control_values):
        """Calculate figure of merit nu given the control value vector as 3 x N."""
        nu = 0
        for u in control_values:
            nu += np.dot(u, u)

        return nu

    def __calculate_gamma(self, control_values):
        """Calculate figure of merit gamma."""
        gamma = 0
        for u, u_p in zip(control_values, control_values[1:,:]):
            du = u_p - u
            gamma += np.dot(du, du)

        return gamma

    def __init_fom_slew(self):
        """Calculate the figures of merit for the complete maneuver."""
        tc    = np.array(self.__co_df['t'])
        idx_n = self._find_threshold_crossing(self.__phi_e, tc)

        if idx_n is not None:
            t_n   = tc[idx_n]
            t_N   = tc[-1]
            dt    = self.__param['simulation']['dt_solve']
            phi_e = self.__phi_e[idx_n:]

            control_values = np.vstack((np.array(self.__co_df['ux']),
                                        np.array(self.__co_df['uy']),
                                        np.array(self.__co_df['uz']))).transpose()

            self.__alpha = self.__calculate_alpha(t_n, t_N, dt, phi_e)
            self.__nu    = self.__calculate_nu(control_values)
            self.__gamma = self.__calculate_gamma(control_values[idx_n:,:])
            self.__phi_e_tn = self.__phi_e[idx_n]

        else:
            self.__t_n_idx = np.inf
            self.__alpha   = np.inf
            self.__nu      = np.inf
            self.__gamma   = np.inf

        self.__phi_e_min = min(self.__phi_e, key=abs)
        self.__phi_e_f   = np.abs(self.__phi_e[-1])
        self.__t_n_idx   = idx_n

    def __init_fom_nadir(self):
        """Calculate the figures of merit for the complete maneuver."""
        self.__init_fom_slew()

    def __init_fom_asi(self):
        """Calculate the figures of merit for each picture acquisition."""
        tc       = np.array(self.__co_df['t'])
        dt_solve = self.__param['simulation']['dt_solve']
        dt_pic   = self.__param['trajectory']['swathwidening']['dt_pic']

        idx_dt_pic = np.where(np.mod(tc,dt_pic) == 0)[0]

        idx_dt_pic = idx_dt_pic[1:]  # remove zero and first picture

        u = np.vstack((np.array(self.__co_df['ux']),
                       np.array(self.__co_df['uy']),
                       np.array(self.__co_df['uz']))).transpose()

        alpha = np.array([])
        nu    = np.array([])
        gamma = np.array([])
        phi_e_min = np.array([])
        phi_e_tn = np.array([])
        phi_e_f   = np.array([])
        t_n_idx   = np.array([])

        for i in range(len(idx_dt_pic)-1):
            phi_e_pa = self.__phi_e[idx_dt_pic[i]:idx_dt_pic[i+1]]
            tc_pa    = tc[idx_dt_pic[i]:idx_dt_pic[i+1]]

            idx_n_pa = self._find_threshold_crossing(phi_e_pa, tc_pa)

            phi_e_min_pa = min(phi_e_pa, key=abs)
            phi_e_f_pa   = np.abs(phi_e_pa[-1])

            u_pa = u[idx_dt_pic[i]:idx_dt_pic[i+1]]

            if idx_n_pa is not None:
                t_n   = tc_pa[idx_n_pa]
                t_N   = tc_pa[-1]
                dt    = self.__param['simulation']['dt_solve']
                phi_e = phi_e_pa[idx_n_pa:]

                a = self.__calculate_alpha(t_n, t_N, dt, phi_e)
                n = self.__calculate_nu(u_pa)
                g = self.__calculate_gamma(u_pa[idx_n_pa:,:])
                phi_e_tn_pa = phi_e[0]

            else:
                idx_n_pa = np.inf
                a = np.inf
                n = np.inf
                g = np.inf
                phi_e_tn_pa = np.inf

            alpha = np.hstack((alpha,a))
            nu    = np.hstack((nu,n))
            gamma = np.hstack((gamma,g))

            phi_e_min = np.hstack((phi_e_min,phi_e_min_pa))
            phi_e_tn = np.hstack((phi_e_tn,phi_e_tn_pa))
            phi_e_f   = np.hstack((phi_e_f,phi_e_f_pa))
            t_n_idx   = np.hstack((t_n_idx, idx_n_pa))
            

        self.__alpha = alpha
        self.__nu    = nu
        self.__gamma = gamma
        self.__phi_e_min = phi_e_min
        self.__phi_e_tn = phi_e_tn
        self.__phi_e_f   = phi_e_f
        self.__t_n_idx   = t_n_idx

    def __init_fom_sssi(self):
        """Calculate the figures of merit for each picture acquisition."""
        tc       = np.array(self.__co_df['t'])
        dt_solve = self.__param['simulation']['dt_solve']
        dt_pic   = self.__param['trajectory']['stereoimaging']['dt_pic']

        idx_dt_pic = np.where(np.mod(tc,dt_pic) == 0)[0]

        idx_dt_pic = idx_dt_pic[1:]  # remove zero and first picture

        u = np.vstack((np.array(self.__co_df['ux']),
                       np.array(self.__co_df['uy']),
                       np.array(self.__co_df['uz']))).transpose()

        alpha = np.array([])
        nu    = np.array([])
        gamma = np.array([])
        phi_e_min = np.array([])
        phi_e_tn = np.array([])
        phi_e_f   = np.array([])
        t_n_idx   = np.array([])

        for i in range(len(idx_dt_pic)-1):
            phi_e_pa = self.__phi_e[idx_dt_pic[i]:idx_dt_pic[i+1]]
            tc_pa    = tc[idx_dt_pic[i]:idx_dt_pic[i+1]]

            idx_n_pa = self._find_threshold_crossing(phi_e_pa, tc_pa)

            phi_e_min_pa = min(phi_e_pa, key=abs)
            phi_e_f_pa   = np.abs(phi_e_pa[-1])

            u_pa = u[idx_dt_pic[i]:idx_dt_pic[i+1]]

            if idx_n_pa is not None:
                t_n   = tc_pa[idx_n_pa]
                t_N   = tc_pa[-1]
                dt    = self.__param['simulation']['dt_solve']
                phi_e = phi_e_pa[idx_n_pa:]

                a = self.__calculate_alpha(t_n, t_N, dt, phi_e)
                n = self.__calculate_nu(u_pa)
                g = self.__calculate_gamma(u_pa[idx_n_pa:,:])
                phi_e_tn_pa = phi_e[0]

            else:
                idx_n_pa = np.inf
                a = np.inf
                n = np.inf
                g = np.inf
                phi_e_tn_pa = np.inf

            alpha = np.hstack((alpha,a))
            nu    = np.hstack((nu,n))
            gamma = np.hstack((gamma,g))

            phi_e_min = np.hstack((phi_e_min,phi_e_min_pa))
            phi_e_tn = np.hstack((phi_e_tn,phi_e_tn_pa))
            phi_e_f   = np.hstack((phi_e_f,phi_e_f_pa))
            t_n_idx   = np.hstack((t_n_idx, idx_n_pa))
            

        self.__alpha = alpha
        self.__nu    = nu
        self.__gamma = gamma
        self.__phi_e_min = phi_e_min
        self.__phi_e_tn = phi_e_tn
        self.__phi_e_f   = phi_e_f
        self.__t_n_idx   = t_n_idx


    def get_spacecraft_telemetry(self):
        """Return the spacecraft.csv data as pandas dataframe."""
        return self.__sc_df

    def get_controller_telemetry(self):
        """Return the controller.csv data as pandas dataframe."""
        return self.__co_df

    def get_actuator_telemetry(self):
        """Return the actuator.csv data as pandas dataframe."""
        return self.__ac_df

    def get_rate_error(self):
        """Return the angular rate error in size 3 x N in deg/s."""
        return self.__w_e

    def get_attitude_error(self):
        """Return the attitude error quaternion in size 4 x N."""
        return self.__q_e

    def get_attitude_error_angle(self):
        """Return the attitude error angle in degree."""
        return self.__phi_e

    def get_min_attitude_error_angle(self):
        """Return the minimal error angle reached (absolute value) in degree."""
        return self.__phi_e_min

    def get_error_threshold_attitude_error_angle(self):
        """Return the error angle at the error threshold crossing (absolute value) in degree."""
        return self.__phi_e_tn

    def get_final_attitude_error_angle(self):
        """Return the final error angle reached (absolute value) in degree."""
        return self.__phi_e_f

    def get_threshold_crossing(self):
        """Return the index at which the threshold is crossed."""
        return self.__t_n_idx

    def get_result_path(self):
        return self.__dname

    def get_simulation_settings(self):
        """Return the simulation settings as dictionary."""
        return self.__param

    def get_alpha(self):
        """Return figure of merit alpha."""
        return self.__alpha

    def get_nu(self):
        """Return figure of merit nu."""
        return self.__nu

    def get_gamma(self):
        """Return figure of merit gamma."""
        return self.__gamma

    def plot(self):
        """Visualize the result for a MPC simulation."""
        plot_result(self.__dname)
        plt.show()


class PostProcessor():
    def __init__(self, directory, result=None):
        """Calculate figures of merits for each result directory given in
        directory.
        
        result: (string) Name of one specific simulation result directory

        If result is given, only post process this one solution.
        """

        self.__single_result = result
        self.__rdir = directory # dirname = 'results/param-study-bs4-slew-2'
        self.__init_result_dirs()
        self.__init_post_process_file()
        self.__post_process()

    def __init_result_dirs(self):
        """Create list of all solution directories in directory.
        If result is given, the list only contains this solution.
        """
        if self.__single_result is None:
            dname = self.__rdir
            rnames = [d for d in os.listdir(dname) 
                        if os.path.isdir(os.path.join(dname, d))]
            self.__rnames = rnames
        elif os.path.isdir(os.path.join(self.__rdir, self.__single_result)):
            self.__rnames = [self.__single_result]
        else:
            KeyError("Can't handle given result: {}".format(self.__single_result))

    def __init_post_process_file(self):
        """Create the file to write the post processing data to.
        If result is given, write it into the solution directory.
        """
        dname = self.__rdir

        if self.__single_result is None:
            pp_file = dname+"/post-process.csv"
        else:
            pp_file = dname+'/'+self.__single_result+'/post-process.csv'

        print("Initialize post processing file: {}\n".format(pp_file))

        with open(pp_file, "w") as f:
            f.write("solution,"
                    + "alpha,nu,gamma,"
                    + "phi_e_min,phi_e_f,"
                    + "hp,"
                    + "Qqs,Qqx,Qqy,Qqz,"
                    + "Qwx,Qwy,Qwz,"
                    + "Qax,Qay,Qaz,"
                    + "Rx,Ry,Rz\n")

        self.__pp_file = pp_file

    def __post_process(self):
        """Calculate

            - figures of merit,
            - min error angle,
            - final error angle
            
            and write it to file.
        """

        progress_bar = ProgressBar(endvalue=len(self.__rnames))
        counter = 0

        for result in self.__rnames:
            R = Result(os.path.join(self.__rdir, result))

            param = R.get_simulation_settings()
            hp = param['mpc']['horizon']
            Qq = param['mpc']['Qq']
            Qw = param['mpc']['Qw']
            Qa = param['mpc']['Qa']
            Ru = param['mpc']['R']

            alpha = np.mean(R.get_alpha())
            nu    = np.mean(R.get_nu())
            gamma = np.mean(R.get_gamma())

            phi_e_min = np.mean(R.get_min_attitude_error_angle())
            phi_e_f   = np.mean(R.get_final_attitude_error_angle())

            with open(self.__pp_file, "a") as f:
                f.write(result+",")

                data = np.hstack((alpha, nu, gamma,
                                  phi_e_min, phi_e_f,
                                  hp, Qq, Qw, Qa[0], Qa[1], Qa[2], Ru,)).reshape(1,-1)
                np.savetxt(f, data, delimiter=',', fmt='%1.16f')
            
            counter += 1
            progress_bar.print_progress_bar(counter)
        print('\n\nSuccessfully proccessed {} solutions.\n\n'.format(counter))

    def get_pp_path(self):
        """Retrun the path to the post process file."""
        return self.__pp_file