import matplotlib.pyplot as plt
import warnings
import numpy as np

def plot_angular_rates(*args, **kwargs):

    if len(args) > 2:
        raise TypeError('Too many input arguments: ' + str(len(args)))

    if len(args) == 1:
        w = args[0]
        t = np.arange(0, len(args[0]), 1)
        x_label = 'Sample'
    else:
        t = args[0]
        w = args[1]
        t_unit = kwargs.pop('t_unit', 'sec')
        x_label = 'Time $t$ [' + t_unit + ']'
        if t_unit == 'min':
            t = t/60
        elif t_unit == 'h':
            t = t/3600

    magnitude = kwargs.pop('magnitude', False)
    show_plot = kwargs.pop('show_plot', False)

    for key, val in kwargs.items():
        unused_argument = ("Unsued but specified argument: \'{0}\' = {1}"
                            .format(key,val))
        warnings.warn(unused_argument)

    w = np.rad2deg(w)

    ax = plt.gca()
    ax.plot(t, w[0:,0], 'r', linewidth=2, label='$\omega_x$')
    ax.plot(t, w[0:,1], 'g', linewidth=2, label='$\omega_y$')
    ax.plot(t, w[0:,2], 'b', linewidth=2, label='$\omega_z$')
    if magnitude:
      w_mag = np.linalg.norm(w, axis=1)
      ax.plot(t, w_mag, 'k', linewidth=2, label=r'$\bar{\omega}$')
    ax.grid()
    ax.legend()
    ax.set_title('Angular Rates')
    ax.set_xlabel(x_label)
    ax.set_ylabel('Angular rate $\omega$ [deg/s]')
    if show_plot:
        plt.draw()

    return ax

