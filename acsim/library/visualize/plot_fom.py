import pandas as pd
import numpy as np

import os
import json

from matplotlib import pyplot as plt
from pyquaternion import Quaternion

from ..util import load_params


def plot_fom(directory, result="", scale=50, id=False, pie_chart=False, colorset1=[], colorset2=[], colorset3=[]):

    pp_dir = os.path.join(directory, result)
    pp_file = os.path.join(pp_dir, 'post-process.csv')

    pp_df = pd.read_csv(pp_file, delimiter=',')

    alpha = np.array(pp_df['alpha'])
    
    # alpha_max = max(alpha[~np.isinf(alpha)])

    fig, ax = plt.subplots()
    size_scale = scale # scale size of the scatter marker

    for index, row in pp_df.iterrows():

        result = row['solution']
        nu     = row['nu']
        gamma  = row['gamma']
        alpha  = row['alpha']

        if alpha == np.inf:
            pass
        else:
            with open(directory+'/'+result+'/parameter.json', 'r') as f:
                params = json.load(f)

            Qq = params['mpc']['Qq'][0]
            Qw = params['mpc']['Qw'][0]
            Qa = params['mpc']['Qa']
            Qah = Qa[::2][0]
            Qaz = Qa[1::2][0]
            R = params['mpc']['R'][0]
            # s = alpha / alpha_max
            # s = 1/s
            # s = np.sqrt(1/alpha)
            s = 1/alpha
            # s = 1
                
            if pie_chart:
                try:
                    total = Qw/Qq + Qah/Qq + Qaz/Qq + R/Qq
                    r1 = Qw/Qq / total
                    r2 = r1 + Qah/Qq / total
                    r3 = r2 + Qaz/Qq / total
                        # R/Qq, residual anyway
                    # Qw/Qq - red
                    ax.scatter(gamma, nu, marker=get_pie_marker(0, r1),  alpha=0.5, s=s ** 2 * size_scale, facecolor='red'  )
                    # Qah/Qq - green
                    ax.scatter(gamma, nu, marker=get_pie_marker(r1, r2), alpha=0.5, s=s ** 2 * size_scale, facecolor='green' )
                    # Qaz/Qq - black
                    ax.scatter(gamma, nu, marker=get_pie_marker(r2, r3), alpha=0.5, s=s ** 2 * size_scale, facecolor='black'   )
                    # R/Qq - blue
                    ax.scatter(gamma, nu, marker=get_pie_marker(r3, 1),  alpha=0.5, s=s ** 2 * size_scale, facecolor='blue' )

                except ZeroDivisionError:
                    ax.scatter(gamma, nu, marker='o', s=s ** 2 * size_scale, edgecolor='red', facecolor='None')
                    if id is True:
                        ax.text(gamma, nu, index)
            else:
                # empty cirlces
                if index in colorset1:
                    ax.scatter(gamma, nu, marker='o', s=s ** 2 * size_scale, edgecolor='red', facecolor='None')
                    ax.set_axisbelow(True)

                elif index in colorset2:
                    ax.scatter(gamma, nu, marker='o', s=s ** 2 * size_scale, edgecolor='blue', facecolor='None')
                    ax.set_axisbelow(True)

                elif index in colorset3:
                    ax.scatter(gamma, nu, marker='o', s=s ** 2 * size_scale, edgecolor='green', facecolor='None')
                    ax.set_axisbelow(True)

                else:
                    ax.scatter(gamma, nu, marker='o', s=s ** 2 * size_scale, edgecolor='black', facecolor='None')
                    ax.set_axisbelow(True)

                if id is True:
                    ax.text(gamma, nu, index, ha="center", va="center")


    if pie_chart:
        # legend entries    
        ax.scatter(0, 0, alpha=0, facecolor='red',   label='$Q_{\omega}/Q_q$')
        ax.scatter(0, 0, alpha=0, facecolor='green', label='$Q_{H_a}/Q_q$')
        ax.scatter(0, 0, alpha=0, facecolor='black', label='$Q_{z_a}/Q_q$')
        ax.scatter(0, 0, alpha=0, facecolor='blue',  label='$R/Q_q$')

        leg = plt.legend(bbox_to_anchor=(1, 1))
        for lh in leg.legendHandles:
            lh.set_alpha(0.5)
    plt.grid()
    plt.xlabel('Control intervention $\\gamma$')
    plt.ylabel('Control effort $\\nu$')
    return fig



def get_pie_marker(r1, r2):
    x = [0] + np.cos(np.linspace(2*np.pi * r1, 2*np.pi * r2, 200)).tolist()
    y = [0] + np.sin(np.linspace(2*np.pi * r1, 2*np.pi * r2, 200)).tolist()
    xy = np.column_stack([x, y])

    return xy