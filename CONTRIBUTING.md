# Contributing

## Setup

### Install Python under Windows
1. Choose version and download using: [https://www.python.org/downloads/windows/](https://www.python.org/downloads/windows/)

2. Run .exe file for installation.

3. Type following command to check the version
    ```python --version```

### Install Poetry under Windows
1.  Install poetry using terminal with command:  
    ```(Invoke-WebRequest -Uri https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py -UseBasicParsing).Content | python -```
2. The command will automatically locate the installation at:  
    ```%USERPROFILE%\.poetry\bin```
3.  To check whether you already have poetry installed or available verion of poetry type:  
    ```poetry --version``` 

### Install Black under Windows
1.  Install black in terminal with command:  
    ```python -m pip install black```
2.  To check whether it is installed:  
    ```black --version```

See https://github.com/psf/black.

### Get the repository
