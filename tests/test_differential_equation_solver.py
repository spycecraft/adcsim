import datetime
import numpy as np
import pytest
import pyquaternion

from adcsim import DifferentialEquationSolver


def test_malformed_begin_datetime_raises_assertion():
    with pytest.raises(ValueError):
        solver = DifferentialEquationSolver(begin="foo")
    with pytest.raises(TypeError):
        solver = DifferentialEquationSolver(begin=7)
    with pytest.raises(TypeError):
        solver = DifferentialEquationSolver(begin=3.5)


def test_malformed_end_datetime_raises_assertion():
    with pytest.raises(ValueError):
        solver = DifferentialEquationSolver(end="bar")
    with pytest.raises(TypeError):
        solver = DifferentialEquationSolver(end=7)
    with pytest.raises(TypeError):
        solver = DifferentialEquationSolver(end=3.5)


def test_negative_time_step_raises_assertion():
    with pytest.raises(ValueError):
        solver = DifferentialEquationSolver(time_step=-0.5)


def test_get_property_angular_rate():
    """A newly constructed differential equation solver returns a 3x1 zero vector as its angular rate vector."""
    des = DifferentialEquationSolver()
    assert isinstance(des.angular_rate, np.ndarray)
    assert np.array_equal(des.angular_rate, np.zeros((3,)))


def test_get_property_attitude():
    """A newly constructed differential equation solver returns a pure unit quaternion as its attitude."""
    des = DifferentialEquationSolver()
    assert isinstance(des.attitude, pyquaternion.Quaternion)
    assert des.attitude == pyquaternion.Quaternion((1, 0, 0, 0))


def test_get_property_inertia():
    """A newly constructed differntial equation solver returns the 3x3 unit matrix as its inertia."""
    des = DifferentialEquationSolver()
    assert isinstance(des.inertia, np.ndarray)
    assert np.array_equal(des.inertia, np.eye(3))
