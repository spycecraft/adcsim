from adcsim import __version__
from adcsim.simulation import DifferentialEquationSolver

import numpy as np
import pytest


def test_version():
    assert __version__ == "0.1.0"


def test_angular_momentum():
    from adcsim.core import AngularMomentum

    H = AngularMomentum(1)
    assert H.angular_momentum == 1


def test_default_simulation():
    solver = DifferentialEquationSolver.from_file()
    t, y = solver.run()

    assert t[-1] - t[0] >= 1.9
    assert len(y) > 10

    # Check that the expected timestamps are in the result
    for i in range(0, 4):
        assert t[0] + i * 0.5 in t


def test_zero_evaluation_steps():
    solver = DifferentialEquationSolver.from_file()
    solver.evaluation_steps = 0
    t, y = solver.run()

    steps = int((t[-1] - t[0]) / solver.time_step) * (1 + solver.evaluation_steps) + 1

    assert len(t) == steps


def test_two_evaluation_steps():
    solver = DifferentialEquationSolver.from_file()
    solver.evaluation_steps = 2
    t, y = solver.run()

    steps = int((t[-1] - t[0]) / solver.time_step) * (1 + solver.evaluation_steps) + 1

    assert len(t) == steps


def test_five_evaluation_steps():
    solver = DifferentialEquationSolver.from_file()
    solver.evaluation_steps = 5
    t, y = solver.run()

    steps = int((t[-1] - t[0]) / solver.time_step) * (1 + solver.evaluation_steps) + 1

    assert len(t) == steps
