from setuptools import setup

setup(
    name="acsim",
    version="1.0",
    description="Attitude Control Simulation",
    url="https://gitlab.tubit.tu-berlin.de/pfda-advanced-control/simulation",
    author="Lennert Hagemann",
    author_email="lennerthagemann@gmail.com",
    license="MIT",
    packages=[
        "acsim",
        "acsim.library",
        "acsim.library.analyze",
        "acsim.library.body",
        "acsim.library.controller",
        "acsim.library.trajectory",
        "acsim.library.util",
        "acsim.library.visualize",
        "acsim.library.actuator",
    ],
    include_package_data=True,
    zip_safe=False,
)
