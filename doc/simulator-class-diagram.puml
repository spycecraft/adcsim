@startuml
skinparam shadowing false

hide circle
hide empty members

frame "Class Diagram: ADCSim" as adcsim {
  package environment {
    class IGRF {
      <<get>> numpy.ndarray magnetic_field
    }
    
    class SGP4 {
      <<get>> Position position
      <<get>> Velocity velocity
    }
    
    class SLPC {
      <<get>> numpy.ndarray sun_vector
    }
    
    SGP4 --o IGRF
  }
  
    class Alignment {
      [[scipy.spatial.transform.Rotation Rotation]] alignment
    }
    
    abstract class Differentiable {
      State state
      derivative()
    }

    abstract class Torque {
      torque
    }
    
  package "sensor" as sensor {
    abstract class Sensor extends Alignment {
      <<get,set>> Float resolution
      <<get,set>> Float[] sampling
    }
    
    class Accelerometer extends Sensor {
      <<get>> numpy.ndarray acceleration_vector
    }
    
    class AngularRateGyroscope extends Differentiable, Sensor {
      <<get>> numpy.ndarray angular_rate_vector
    }
    
    class MagneticFieldSensor extends Sensor {
      <<get>> numpy.ndarray magnetic_field_vector
    }
    
    class SunSensor extends Sensor {
      <<get>> numpy.ndarray sun_vector
    }
  }
  
  IGRF --o MagneticFieldSensor
  SLPC --o SunSensor

  package "perturbation" as perturbations {
    abstract class Perturbation extends Torque
    
    class AerodynamicTorque extends Perturbation{
      AerodynamicTorque()
    }

    class SolarPressureTorque extends Perturbation {
      SolarPressureTorque()
    }

    class GravityGradientTorque extends Perturbation {
      GravityGradientTorque()
    }

    class ResidualMagneticTorque extends Perturbation {
      ResidualMagneticTorque()
    }
  }
  
  SGP4 --o AerodynamicTorque
  IGRF ---o ResidualMagneticTorque
  SLPC --o SolarPressureTorque

  package "actuator" as actuator {
    abstract class AngularMomentum {
      angular_momentum
    }

    abstract class Input {
      input
    }

    abstract class Actuator extends Alignment, Input, Torque
    note top of Actuator : Maybe easier, if all actuators extend\nTorque, Input, AngularMomentum,\nand Differentiable?

    class FluidDynamicActuator extends Actuator, Differentiable, AngularMomentum {
      FluidDynamicActuator(K, T)
    }

    class Magnetorquer extends Actuator {
      Magnetorquer(dipole, alignment)
    }

    class MomentumBiasWheel extends AngularMomentum {
      MomentumBiasWheel(angular_momentum)
      MomentumBiasWheel(angular_velocity, inertia)
    }

    note top of MomentumBiasWheel : Has no input,\nonly angular\nmomentum?

    class ReactionWheel extends Actuator, Differentiable, AngularMomentum {
      ReactionWheel()
    }
  }

  package motion {
    class QuaternionKinematics extends Differentiable {
      [[https://kieranwynn.github.io/pyquaternion/ Quaternion]] quaternion
      update(angular_rate)
    }

    class RigidBodyDynamics extends Differentiable {
      AngularRate angular_rate
      Inertia inertia
      RigidBodyDynamics(inertia, angular_rate)
      update(Torque[], AngularMomentum[])
    }
  }

  package simulation {
    abstract class DifferentialEquationSolver {
      DifferentialEquationSolver()
    }
    
    class FloatSatSimulation extends DifferentialEquationSolver {
      FloatSatSimulation()
      from_file(String filename)
      rhs(Time t, State x)
    }
    
    class RocketSimulation extends DifferentialEquationSolver {
      RocketSimulation()
      from_file(String filename)
      rhs(Time t, State x)
    }
    
    class SatelliteSimulation extends DifferentialEquationSolver {
      SatelliteSimulation()
      from_file(String filename)
      rhs(Time t, State x)
    }
  }
  DifferentialEquationSolver "1" *----- "many" Actuator : "actuators"
  DifferentialEquationSolver "1" *---- "many" Sensor : "sensors"
  
  Perturbation "many" -* "1" DifferentialEquationSolver : "perturbations"
  QuaternionKinematics "1" --* "1" DifferentialEquationSolver : "kinematics"
  RigidBodyDynamics "1" --* "1" DifferentialEquationSolver : "dynamics"
}
@enduml
